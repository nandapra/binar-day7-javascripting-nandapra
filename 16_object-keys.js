const car = {
    make: "Honda",
    model: "Accord",
    year: 2020
}

const keys = Object.keys(car)

const value = keys.map(mapper)

function mapper(key){
    return car[key]
}

console.log(keys)
console.log(value)